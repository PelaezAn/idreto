'use express';
const colors = require('colors');
const path = require('path');
const React = require('react');
const ReactDOMServer = require('react-dom/server');
const express = require('express');
const passport = require('passport');
const querystring = require('querystring');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const passportConfig = require('./config/passport');
const app = express();
const priceService = require('./services/prices');

app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, ".")));

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    return res.redirect(`/login?${querystring.stringify({error: 'unauthorized'})}`);
}

app.use(cookieParser()); 
app.use(session({
    secret: process.env.SESSION_SECRET || 'idreto',
    resave: true,
    saveUninitialized: true
}));

passportConfig(passport);
app.use(passport.initialize());
app.use(passport.session());


app.get('/login', function(req, res) {
    if (!req.query.error) {
        return res.render('login');
    } else if (req.query.error === 'unauthorized') {
        return res.render('login', { error: "Invalid petition, please sign in again." })
    } else if (res.query.error === 'internal-error') {
        return res.render('login', { error: "There's been an error. Please make sure your application is correctly configured and try again." })
    } 
    res.render('login')
});

app.get('/logout', function(req, res) {
    req.logout();
    return res.redirect('/login');
});

app.get('/', isLoggedIn, (req, res) => res.render('index', { user: req.user }));

app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));
app.get('/auth/google/callback', function(req, res, next) {
    passport.authenticate('google', function(error, user) {
            if (error) { 
                return res.redirect(`/login?${querystring.stringify({error: 'internal-error'})}`);
            }
            if (!user) { 
                return res.redirect(`/login?${querystring.stringify({error: 'unauthorized'})}`);
            }
            req.logIn(user, function(error) {
                if (error) { 
                    return res.redirect(`/login?${querystring.stringify({error: 'internal-error'})}`);
                }
                return res.redirect('/');
            });
    })(req, res, next);
});

app.get('/cryptocurrency', isLoggedIn, async function (req, res) {
    try {
        const result = await priceService.requestLast24HoursData(req.query.symbol);
        res.send(result)
    } catch(error) {
        res.status(500).send();
    }
});

app.listen(3000, () => console.log('Server started'.green));

