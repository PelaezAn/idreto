# Irdeto technical assessment

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Node.js vs 8.9.0 or higher

### Configuring

####Google O-Auth2

This application uses Google O-Auth2 and in order to work requires the following environment variables to be set:

| Environment variable  | Description | Default
| ------------- | ------------- | ------------- |
| GOOGLE_CLIENT_ID  | Google application ID  | - |
| GOOGLE_CLIENT_SECRET  | Google application secret  | - |
| GOOGLE_AUTH_CALLBACK  | Google authorization callback endpoint | http://localhost:3000/auth/google/callback |

#### Cookies secret

It is also necessary to set a secret for the session cookies.

| Environment variable  | Description |
| ------------- | ------------- |
| SESSION_SECRET  | Secret for the application cookies  |

### Installing

To get the project running you will need to follow the following steps. Install all required modules using the node package manager in the root folder.

```
npm i
```

You will also need to bundle all client-side javascript files. You can do so with the following command in the same directory.

```
webpack
```

Finally you just need to start the server using

```
npm start
```

And access the project on the following address

```
http://localhost:3000
```

### Part 2 of the assignment

Part 2 of the assignment can be found in the file part_2_assesment_Irdeto.pdf
