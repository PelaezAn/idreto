module.exports = (function() {
    "use strict";
    const request = require( 'request' );
    const BASE_URL = "https://poloniex.com/public";
    const FIVE_MINUTES = '300';
    const COMMAND = 'returnChartData';
    const END_OF_TIME = '9999999999';


    /**
     * 
     * @return {String}
     */
    function _twentyFourHoursAgo() {
        return `${(new Date(new Date().getTime() - (24 * 60 * 60 * 1000))).getTime()/1000}`
    }

    /**
     * 
     * @param {String} command
     * @param {String} currencyPair
     * @param {String} start
     * @param {String} end
     * @param {String} period
     * @return {Promise}
     */
    function _request(command, currencyPair, start, end, period) {
        return new Promise((resolve, reject) => {
            request.get(
                { url : BASE_URL, qs : { command, currencyPair, start, end, period }, json : true },
                ( err, result ) => {
                    if (err || !result.body) {
                        return reject( new Error( err || "NO DATA FOUND"));
                    }
                    return resolve(result.body);
                });
        });
    }

    /**
     * 
     * @param {String} currency 
     */
    function requestLast24HoursData(currency) {
        return new Promise(async (resolve, reject) => {
            try {
                if (typeof currency !== 'string' || ['LSK', 'ETH', 'XMR', 'STRAT', 'BCH'].indexOf(currency) === -1) {
                    return reject(new Error("Invalid currency"));
                }
                const response = await _request(COMMAND, `BTC_${currency}`, _twentyFourHoursAgo(), END_OF_TIME, FIVE_MINUTES);
                resolve(response);
            } catch(error) {
                reject(error);
            }
        });
    }

    return { requestLast24HoursData };
})();
