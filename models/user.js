module.exports = (function() {
    "use strict";

    /*
        For this project we will keep the users in memory (thus accesing them synchronously). Obviously in a more serious project should be
        stored in a db
     */

    let users = {};

    /**
     *
     * @param {String} email
     * @return {Object|null}
     */
    function findUserByEmail(email) {
        return users[email] || null;
    }

    /**
     * 
     * @param {Object} profile 
     */
    function addNewUser(profile) {
        let user = {
            id: profile.id,
            name: profile.displayName,
            token: profile.token,
            email: profile.email
        };
        users[profile.email] = user;
        return user;
    }

    return {findUserByEmail, addNewUser};
})();