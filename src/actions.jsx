"use strict";
import actionTypes from "./actionTypes";
import {fetchSymbolData} from "./lib/api";
/**
 * 
 * @param {Number} threshold 
 */
export function setThreshold(threshold) {
    return {type: actionTypes.SET_THRESHOLD, payload: threshold}
}

/**
 * 
 * @param {String} symbol 
 */
function selectCurrency(symbol) {
    return {type: actionTypes.SELECT_CRYPTOCURRENCY, payload: symbol}
}

/**
 * 
 * @param {String} symbol 
 * @param {Object} data 
 */
function setSymbolData(symbol, data) {
    return {type: actionTypes.SET_SYMBOL_DATA, payload: {symbol, data}}
}

/**
 * 
 * @param {String} symbol 
 */
export function requestCurrencyData(symbol) {
    return async function(dispatch, getState) {
        try {
            dispatch(selectCurrency(symbol));
            const { data } = getState();
            if (_.isEmpty(data[symbol])) {
                const response = await fetchSymbolData(symbol);
                if(!response.ok) {
                    dispatch(setSymbolData(symbol, null));
                }
                const symbolData = await response.json();
                dispatch(setSymbolData(symbol, symbolData));
            }
        } catch(error) {
            dispatch(setSymbolData(symbol, null));
            console.error(error);
        }
    }
}
