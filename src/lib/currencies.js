"use strict";

const currencies = [
    { name: 'Lisk', symbol: 'LSK'},
    { name: 'Ethereum', symbol: 'ETH'},
    { name: 'Monero', symbol: 'XMR'},
    { name: 'Stratis', symbol: 'STRAT'},
    { name: 'Bitcoin Cash', symbol: 'BCH'}
];

export default currencies;