"use strict";
import 'whatwg-fetch'

/**
 * 
 * @param {String} symbol 
 * @return {Promise}
 */
export async function fetchSymbolData(symbol) {
    return window.fetch(`/cryptocurrency?symbol=${symbol}`, {credentials: 'same-origin'});
}
