import currencies from "./lib/currencies";

"use strict";

export default {
    threshold: 0,
    data: {},
    user: window.user,
    selectedCurrency: currencies[0].symbol
};
