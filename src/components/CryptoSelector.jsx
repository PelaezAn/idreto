"use strict";
import React from "react";
import { connect } from "react-redux";
import _ from "lodash";
import currencies from "../lib/currencies";
import {requestCurrencyData} from "../actions";

const Crypto = (props) => {
    const { currency } = props;
    return (
        <option value={currency.symbol}>{`${currency.name} (${currency.symbol})`}</option>
    );
};

class CryptoSelector extends React.PureComponent {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    /**
     * 
     * @param {Event} event 
     */
    handleChange(event) {
        this.props.selectCryptoCurrencyAction(event.target.value);
    }

    componentDidMount() {
        const { selectedCurrency, selectCryptoCurrencyAction } = this.props;
        selectCryptoCurrencyAction(selectedCurrency);
    }

    render() {
        return(
            <div className="crypto-selector medium-container">
                <h1>Select cryptocurrency</h1>
                <select value={this.props.selectedCurrency} onChange={this.handleChange}>
                    {
                        _.map(currencies, (currency) => <Crypto currency={currency} />)
                    }
                </select>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        selectedCurrency: state.selectedCurrency
    };
}

function mapDispatchToProps(dispatch) {
    return {
        selectCryptoCurrencyAction: (symbol) => dispatch(requestCurrencyData(symbol))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CryptoSelector);
