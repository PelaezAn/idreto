"use strict";
import React from "react";
import _ from "lodash";
import { connect } from "react-redux";
import { setThreshold } from "../actions";

class ThresholdSelector extends React.PureComponent {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    /**
     * 
     * @param {Event} event 
     */
    handleChange(event) {
        const { value } = event.target; 
        if (!isNaN(value)) {
            this.props.selectThresholdAction(value);
        }
    }

    render() {
        return(
            <div className="treshold-selector medium-container">
                <h1>Enter Threshold</h1>
                <input type="numeric" value={this.props.threshold} onChange={this.handleChange}/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        threshold: state.threshold
    };
}

function mapDispatchToProps(dispatch) {
    return {
        selectThresholdAction: (threshold) => dispatch(setThreshold(threshold))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ThresholdSelector);
