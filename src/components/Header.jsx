"use strict";
import React from "react";
import { connect } from "react-redux";

class Header extends React.PureComponent {

    constructor() {
        super();
        this.logout = this.logout.bind(this);
    }

    /**
     * 
     * @param {Event} event 
     */
    logout(event) {
        event.preventDefault();
        event.stopPropagation();
        this.props.logOutAction();
    }

    render() {
        const { user } = this.props;
        return(
            <div className="row">
                <div className="header-action">
                    <span>{`Welcome ${user.name || user.email} | `}</span>
                    <a href="/logout">Logout</a>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    };
}

export default connect(mapStateToProps)(Header);
