"use strict";
import React from "react";
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import _ from "lodash";
import { connect } from "react-redux";
import { setThreshold } from "../actions";

const PriceRow = (props) => {
    const { price } = props;

    return(
        <tr>
            <td>{ (new Date(price.date * 1000)).toLocaleDateString() }</td>
            <td>{ price.high }</td>
            <td>{ price.low }</td>
            <td>{ price.volume }</td>
        </tr>
    );
};

class ResultsTable extends React.PureComponent {

    render() {
        const { threshold, data, selectedCurrency } = this.props;
        const selected = data[selectedCurrency];
        let content;

        if (typeof selected === 'undefined') {
            content = <td colSpan="4"> Retrieving data ... </td>
        } else if (!selected) {
            content = <td className="fetching-error" colSpan="4"> Data couldn't be retrieved </td>
        } else {
            const filteredData = _.filter(selected, (price) => price.high > threshold);
            if (filteredData.length) {
                content = _.map(filteredData, (price, index) => <PriceRow price={price} key={`${selectedCurrency}-${price.date}`} />);
            } else {
                content = <td colSpan="4"> No data matches the given criteria </td>
            }
        }

        return(

            <div className="results-table row">
                <table>
                    <tr>
                        <th>time (5m interval)</th>
                        <th>high</th>
                        <th>low</th>
                        <th>volume</th>
                    </tr>
                    { content }
                </table>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        threshold: state.threshold,
        data: state.data,
        selectedCurrency: state.selectedCurrency
    };
}

export default connect(mapStateToProps)(ResultsTable);
