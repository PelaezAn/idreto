"use strict";
import React from "react";
import Header from "components/Header";
import CryptoSelector from "components/CryptoSelector";
import ThresholdSelector from "components/ThresholdSelector";
import ResultsTable from "components/ResultsTable";

const App = () => {
  return (
        <div>
            <Header />
            <h2 className="row app-title">
                Cryptocurrency Threshold Evaluator (past 24hrs)
            </h2>
            <div className="row">
                <CryptoSelector />
                <ThresholdSelector />
            </div>
            <ResultsTable />
            <h5 class="row footer">Idreto technical assesment 2018</h5>
        </div>
    );
};

export default App;
