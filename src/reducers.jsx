"use strict";
import initialState from './initialState';
import actionTypes from "./actionTypes";

const reducers = ( state = initialState, action = '' ) => {
    switch ( action.type ) {
        case actionTypes.SELECT_CRYPTOCURRENCY: 
            return { ...state, selectedCurrency: action.payload };
        case actionTypes.SET_THRESHOLD: {
            return { ...state, threshold: action.payload };
        }
        case actionTypes.SET_SYMBOL_DATA: {
            const data = { ...state.data, [action.payload.symbol]: action.payload.data };
            console.log(action.payload);
            return { ...state, data };
        }
        default :
            return { ...state };
    }
};

export default reducers;
