"use strict";

module.exports = {
    'googleAuth' : {
        'clientID'      : process.env.GOOGLE_CLIENT_ID ,
        'clientSecret'  : process.env.GOOGLE_CLIENT_SECRET,
        'callbackURL'   : process.env.GOOGLE_AUTH_CALLBACK ||'http://localhost:3000/auth/google/callback'
    }
};
