module.exports = function(passport) {
    "use strict";

    const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
    const configAuth = require('./auth');
    const User = require('../models/user');

    passport.serializeUser(function(user, done) {
        done(null, user.email);
    });

    passport.deserializeUser(function(email, done) {
        done(null, User.findUserByEmail(email));
    });

    passport.use(new GoogleStrategy(configAuth.googleAuth,
        function(token, refreshToken, profile, done) {
            process.nextTick(function() {
                let email = profile.emails[0].value;
                let user = User.findUserByEmail(email);
                if (user) {
                    return done(null, user);
                } else {
                    let newUser = {
                        id: profile.id,
                        name: profile.displayName,
                        token: token,
                        email: email
                    };

                    User.addNewUser(newUser);
                    return done(null, newUser);
                }
            });

        }));

};